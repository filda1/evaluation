import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})


export class SidebarComponent implements OnInit {

  public uiBasicCollapsed = false;
  public samplePagesCollapsed = false;

  public samplePagesCollapsed1 = false;
  public samplePagesCollapsed2 = false;
  public samplePagesCollapsed3 = false;
  public samplePagesCollapsed4 = false;
  public samplePagesCollapsed5 = false;
  public samplePagesCollapsed6 = false;
  public samplePagesCollapsed7 = false;
  public samplePagesCollapsed8 = false;
  public samplePagesCollapsed9 = false;
  public samplePagesCollapsed10 = false;
  public samplePagesCollapsed11 = false;
  public samplePagesCollapsed12 = false;
  public samplePagesCollapsed13 = false;
  

  constructor(public translate: TranslateService) { }


  ngOnInit() {
    const body = document.querySelector('body');


    // add class 'hover-open' to sidebar navitem while hover in sidebar-icon-only menu
    document.querySelectorAll('.sidebar .nav-item').forEach(function (el) {
      el.addEventListener('mouseover', function() {
        if(body.classList.contains('sidebar-icon-only')) {
          el.classList.add('hover-open');
        }
      });
      el.addEventListener('mouseout', function() {
        if(body.classList.contains('sidebar-icon-only')) {
          el.classList.remove('hover-open');
        }
      });
    });
  }

}
